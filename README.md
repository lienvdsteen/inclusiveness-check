# Inclusiveness Check

This project uses the Ruby gem [Linter](https://gitlab.com/lienvdsteen/linter). A library heavily inspired by [Gender Decoder](http://gender-decoder.katmatfield.com/).

The goal is to add additional checks to the gem (and this tool) to make sure text can be as inclusive as possible.

Try it out [here](https://inclusiveness-check.herokuapp.com/).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/lienvdsteen/inclusiveness-check. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Inclusiveness Check project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/lienvdsteen/inclusiveness-check/linter/blob/master/CODE_OF_CONDUCT.md).
