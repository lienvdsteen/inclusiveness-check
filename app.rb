require "sinatra"
require "sinatra/reloader" if development?
require 'linter'

class App < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  get "/" do
    erb :index, locals: {text: '', job: false, job_result: false}
  end

  post '/check' do
    text = params.dig('text')
    job_ad = params.has_key?('job')
    result = Linter.analyze(text)
    job_result = Linter::MindsetAssociation.analyze(text) if job_ad

    erb :index, locals: {
      text: text,
      result: result,
      job_result: job_ad ? job_result : false
    }
  end

  get '/about' do
    sources = Linter.sources
    erb :about, locals: { sources: sources }
  end

  get '/gender-neutral' do
    erb :'gender-neutral'
  end
end
